﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Coronitor.Models;
using static System.Net.WebRequestMethods;
using Coronitor.Data;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Coronitor.Controllers
{
    public class AccountController : Controller
    {
        private readonly CoronitorContext _context;

        public static string UNAUTHORIZED_MESSAGE = "You have no permissions for this action!";
        public AccountController(CoronitorContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            ViewBag.error = false;
            return View();
        }

        [HttpPost]
        public IActionResult Login(LoginViewModel @login)
        {
            ViewBag.error = false;
            if (ModelState.IsValid)
            {
                var user = _context.User.Include(u => u.JobType)
                                        .FirstOrDefault(u => u.Id == login.Id &&
                                                        u.Password == login.Password);
                if (user != null)
                {
                    HttpContext.Session.SetInt32("id", user.Id);

                    if (user.Admin)
                    {
                        HttpContext.Session.SetString("admin", "admin");
                    }

                    HttpContext.Session.SetInt32("jobtype", user.JobType.Id);
                    HttpContext.Session.SetString("firstname", user.FirstName);
                    return Redirect("~/Home/Index");

                }
                else
                {
                    ViewBag.error = true;
                    return View("Index");
                }
            }
            ViewBag.error = true;

            return View("Index");
        }

        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.Session.Remove("id");
            HttpContext.Session.Remove("jobtype");
            HttpContext.Session.Remove("admin");
            HttpContext.Session.Remove("firstname");
            return Redirect("~/Home/Index");
        }

        public static bool IsUserConnected(Controller controller)
        {
            return controller.HttpContext.Session.GetInt32("id") != null;
        }


        public static bool IsAdminUserConnected(Controller controller)
        {
            return IsUserConnected(controller) && controller.HttpContext.Session.GetString("admin") != null;
        }

        public static bool IsUserOfType(Controller controller, JobTypeEnum jobType)
        {
            return controller.HttpContext.Session.GetInt32("jobtype") == (int)jobType;
        }
    }
    }
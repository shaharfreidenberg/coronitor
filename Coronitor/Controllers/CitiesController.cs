﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Coronitor.Data;
using Coronitor.Models;
using OpenCage.Geocode;
using System.Text.Json;
using System.Data;
using Microsoft.AspNetCore.Http;

namespace Coronitor.Controllers
{
    public class CitiesController : Controller
    {
        private readonly CoronitorContext _context;
        private static int CitySequnce = 0;
        private static string GEO_CODER_API_KEY = "64afde3b45f14dc08cce2e98089f3c70";
        private static int LONG_LAT_DEFAULT_VALUE = 0;

        private async Task<string> GetNextCityIDAsync()
        {
            int maxSequence = 1;
            var cities = await _context.City.ToListAsync();

            foreach (City city in cities)
            {
                if(int.Parse(city.Id) > maxSequence)
                {
                    maxSequence = int.Parse(city.Id);
                }
            }
            maxSequence++;

            return (maxSequence.ToString());
        }

   
    private async Task InsertCitiesName()
        {
            List<string> cities = new List<string>(){
"מודיעין עילית",
"מטולה",
"מעלה אדומים",
"מעלות-תרשיחא",
"נהריה",
"נס ציונה",
"נצרת עילית",
"נשר",
"נתיבות",
"נתניה",
"עכו",
"עפולה",
"ערד",
"פתח תקווה",
"צפת",
"קריית אונו",
"קריית ארבע",
"קריית אתא",
"קריית ביאליק",
"קריית גת",
"קריית ים",
"קריית מוצקין",
"קריית מלאכי",
"קריית שמונה",
"קריית טבעון",
"ראש העין",
"ראש פינה",
"ראשון לציון",
"רחובות",
"רמלה",
"רמת גן",
"רמת השרון",
"רעננה",
"שדרות",
"תל אביב",
"אופקים",
"אור יהודה",
"אור עקיבא",
"אילת",
"אלעד",
"אריאל",
"אשדוד",
"אשקלון",
"באר שבע",
"בית שאן",
"בית שמש",
"ביתר עילית",
"בני ברק",
"בת ים",
"גבעת שמואל",
"גבעתיים",
"דימונה",
"הוד השרון",
"הרצליה",
"זכרון יעקב",
"חדרה",
"חולון",
"חיפה",
"טבריה",
"טירת כרמל",
"יבנה",
"יהוד-מונוסון",
"יפו",
"יקנעם",
"ירושלים",
"כפר סבא",
"כרמיאל",
"לוד",
"מגדל העמק",
"מודיעין-מכבים-רעות" };
            cities.ForEach(async city =>
            {
                _context.Add(new City
                {
                    Name = city,
                    Id = await GetNextCityIDAsync(),
                    Longitude = LONG_LAT_DEFAULT_VALUE,
                    Latitude = LONG_LAT_DEFAULT_VALUE,
                });
                CitySequnce++;
            });

            // Save changes to DB
            _context.SaveChanges();
        }

        public CitiesController(CoronitorContext context)
        {
            _context = context;
        }

        // GET: Cities
        public async Task<IActionResult> Index(string cityName)
        {

            if (AccountController.IsAdminUserConnected(this))
            {
                cityName = (cityName == null ? "" : cityName);

                if (_context.City.Count() < 1)
                    await InsertCitiesName();

                await LoadCitiesLocationAsync();


                var cities =
                    _context.City.Where(city => city.Name.Contains(cityName));

                return View(await cities.ToListAsync());
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
          
        }
        private async Task LoadCitiesLocationAsync()
        {
           
            // Gets all the cities that we dont have locations for
            var cities = await _context.City.Where(s => s.Longitude == LONG_LAT_DEFAULT_VALUE &&
                                                s.Latitude == LONG_LAT_DEFAULT_VALUE).ToListAsync();
            
            cities.ForEach(city => LoadCityCordinates(city));

            _context.SaveChanges();
        }

        private bool LoadCityCordinates(City city)
        {

            bool isLoadingWasSuccsuscel = false;

            Geocoder gc = new Geocoder(GEO_CODER_API_KEY);

            var citySearchResult = gc.Geocode(city.Name);

            // Checks if we got result for this city
            if (citySearchResult != null && 
                citySearchResult.Results.Length > 0 &&
                citySearchResult.Results[0].Components.CountryCode == "il")
            {
                city.Latitude = citySearchResult.Results[0].Geometry.Latitude;
                city.Longitude = citySearchResult.Results[0].Geometry.Longitude;
                isLoadingWasSuccsuscel = true;
            }

            return (isLoadingWasSuccsuscel);
        }
        // GET: Cities/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                if (id == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                var city = await _context.City
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (city == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                return View(city);
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
          
        }

        public async Task<IActionResult> GetNumberOfSickPeopleInEveryCity()
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                // Get all sick events
                var eventPatientCities = await _context.Event.Include(x => x.Patient)
                    .ThenInclude(p => p.City).Where(e => e.IsSick).ToListAsync();

                // Get all cities
                var cities = await _context.City.ToListAsync();

                Dictionary<string, int> map = new Dictionary<string, int>();

                // Map the number of infections in every city
                foreach (var cityInfection in
                   eventPatientCities.GroupBy(e => e.Patient.City.Id)
                   .Select(g => new { cityID = g.Key, count = g.Count() }))
                {
                    map[cityInfection.cityID] = cityInfection.count;
                }

                List<Object> citiesAndInfections = new List<Object>();

                // Create the respond to the map view
                foreach (var city in cities)
                {
                    int numOfInfections = 0;

                    if (map.ContainsKey(city.Id))
                    {
                        numOfInfections = map[city.Id];
                    }

                    citiesAndInfections.Add(new
                    {
                        city.Name,
                        numOfInfections = numOfInfections,
                        city.Longitude,
                        city.Latitude
                    });

                }

                return new JsonResult(citiesAndInfections);
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
          
        }

        // GET: Cities/Create
        public IActionResult Create()
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                ViewBag.loadingFailed = false;
                ViewBag.cityAlredyExist = false;


                return View();
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
           
        }

        // POST: Cities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,InfectedNum")] City city)
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                ViewBag.loadingFailed = false;
                ViewBag.cityAlredyExist = false;

                if (ModelState.IsValid)
                {
                    if (_context.City.Where(currCity => currCity.Name == city.Name).ToListAsync().Result.Count >= 1)
                    {
                        ViewBag.cityAlredyExist = true;
                        return View();
                    }
                    else
                    {
                        city.Id = await GetNextCityIDAsync();
                        if (LoadCityCordinates(city))
                        {
                            _context.Add(city);
                            await _context.SaveChangesAsync();
                            return View(city);
                        }

                        ViewBag.loadingFailed = true;
                    }
                }

                return View();
            } else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }

        }

        // GET: Cities/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (AccountController.IsAdminUserConnected(this)) { 
                if (id == null)
                {
                    return View("~/Views/Shared/Error.cshtml");

                }

            var city = await _context.City.FindAsync(id);
            if (city == null)
            {
                return View("~/Views/Shared/Error.cshtml");

            }
            return View(city);
            } else {
                return View("~/Views/Shared/401Error.cshtml");
            }
        }

        // POST: Cities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Name,Longitude,Latitude,Id,InfectedNum")] City city)
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                if (id != city.Id)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        _context.Update(city);
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!CityExists(city.Id))
                        {
                            return View("~/Views/Shared/Error.cshtml");
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return RedirectToAction(nameof(Index));
                }
                return View(city);
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
           
        }

        // GET: Cities/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                string failed = "cityDeletedFailed" + id;
                ViewData.Add(failed, false);

                if (id == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                var city = await _context.City
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (city == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                return View(city);
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
           
        }

        // POST: Cities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                var city = await _context.City.FindAsync(id);

                try
                {
                    _context.City.Remove(city);
                    _context.SaveChanges();

                }
                catch (Exception)
                {
                    string failed = "cityDeletedFailed" + id;
                    ViewData.Add(failed, true);
                    return View(city);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
            
        }

        private bool CityExists(string id)
        {
            return _context.City.Any(e => e.Id == id);
        }
    }
}

﻿using Coronitor.Data;
using Coronitor.Migrations;
using Coronitor.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coronitor.Controllers
{
    public class EventsController : Controller
    {
        private readonly CoronitorContext _context;

        public EventsController(CoronitorContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index(Boolean IsSick, DateTime EventDate, int Status = -1)
        {

            var events = GetEruimByPermission().Cast<Event>().Where(e => (Status == -1 || e.Status == (EventStatus)Status) &&
                                                                         (!IsSick || e.IsSick == IsSick) && 
                                                                          e.CreationTime >= EventDate);

            events = Calculate(events);
            return View(await events.ToListAsync());
        }

        private IQueryable<Event> Calculate(IQueryable<Event> events)
        {
            foreach (var item in events)
            {
                // get the symptoms of the current event 
                var symptomsId = _context.EventSymptoms.Where(e => e.EventId == item.Id).Select(e => e.SymptomId);
                // get the events that has at least one symptom of the current event symptoms
                var all = _context.EventSymptoms.Where(es => symptomsId.Contains(es.SymptomId));
                // get the count of the sick people
                var count = all.Where(e => e.Event.IsSick).Select(e => e.EventId).Distinct().Count();
                // calculate chance to be sick
                item.ChanchToBeSick = all.Count() != 0 ? (count * 100) / all.Select(e => e.Event).Distinct().Count() : -1;
            }

            return events;
        }


        private IQueryable GetEruimByPermission()
        {
            var jobid = HttpContext.Session.GetInt32("jobtype");
            var userid = HttpContext.Session.GetInt32("id");
            var query = _context.Event.Include(e => e.Doctor)
                .Include(e => e.Patient)
                .Include(e => e.Symptoms).ThenInclude(es => es.Symptom);

            if (HttpContext.Session.GetString("admin") != null)
            {
                return query;
            }
            else if (jobid == (int)JobTypeEnum.Patient)
            {
                return query.Where(e => e.Patient.Id == userid);
            }
            else if (jobid == (int)JobTypeEnum.Doctor)
            {
                return query.Where(e => e.Doctor.Id == userid);
            }
            else if (jobid == (int)JobTypeEnum.LaboratoryDirector)
            {
                return query;
            }
            return null;
        }

        // get create view
        public IActionResult Create()
        {
            ViewBag.Doctors = new SelectList(_context.User.Where(u => u.JobType.Id == (int)JobTypeEnum.Doctor), "Id", "FullName");
            ViewBag.Symptoms = new SelectList(_context.Symptom.ToList(), "Id", "Name");
            if(!(HttpContext.Session.GetInt32("jobtype") == (int)JobTypeEnum.Patient))
            {
                ViewBag.Patients = new SelectList(_context.User.Where(u => u.JobType.Id == (int)JobTypeEnum.Patient), "Id", "FullName");
            }

            return View();
        }

        // create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Temperature")] Event @event, List<int> Symptoms, int Doctor, int? Patient = null)
        {
            if (ModelState.IsValid)
            {
                int userId = (HttpContext.Session.GetInt32("id").HasValue ? (int)HttpContext.Session.GetInt32("id") : 123456789);
                @event.CreationTime = DateTime.Now;
                @event.UpdateTime = DateTime.Now;
                @event.Status = EventStatus.New;
                @event.Patient = Patient.HasValue 
                    ? _context.User.Where(u => u.Id == Patient).FirstOrDefault()
                    : _context.User.Where(u => u.Id == userId).FirstOrDefault();
                @event.Doctor = _context.User.Where(u => u.Id == Doctor).FirstOrDefault();
                var e =_context.Add(@event);
                _context.SaveChanges();
                foreach (var symptom in Symptoms)
                {
                    var es = new EventSymptom() { SymptomId = symptom, EventId = e.Entity.Id };
                    _context.Add(es);
                }
                _context.SaveChanges();

                return RedirectToAction(nameof(Index));
            }
            return View(@event);
        }

        // get edit view
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return View("~/Views/Shared/Error.cshtml");
            }

            var @event = await GetEruimByPermission().Cast<Event>()
                .FirstOrDefaultAsync(x => x.Id == id);
            
            if (@event == null)
            {
                return View("~/Views/Shared/Error.cshtml");
            }

            var model = new EditEventViewModel
            {
                Id = @event.Id,
                Patient = @event.Patient,
                IsSick = @event.IsSick,
                DoctorId = @event.Doctor.Id,
                Status = @event.Status,
                SymptomsIds = @event.Symptoms.Select(s => s.SymptomId).ToArray(),
                Temperature = @event.Temperature,
                TestDate = @event.TestDate
            };

            var doctorsLst = new List<SelectListItem>();
            var statusesLst = new List<SelectListItem>();
            var symptomsLst = new List<SelectListItem>();
            var doctors = _context.User.Where(u => u.JobType.Id == (int)JobTypeEnum.Doctor).ToList();
            var statuses = Enum.GetValues(typeof(EventStatus)).Cast<EventStatus>();
            var symptoms = _context.Symptom.ToList();

            foreach (var doctor in doctors)
            {
                doctorsLst.Add(new SelectListItem
                {
                    Text = doctor.FullName,
                    Value = doctor.Id.ToString(),
                });
            }

            foreach (var status in statuses)
            {
                statusesLst.Add(new SelectListItem
                {
                    Text = Enum.GetName(typeof(EventStatus), status),
                    Value = status.ToString(),
                });
            }

            foreach (var symptom in symptoms)
            {
                symptomsLst.Add(new SelectListItem
                {
                    Text = symptom.Name,
                    Value = symptom.Id.ToString(),
                });
            }

            model.DoctorsLst = doctorsLst;
            model.StatusesLst = statusesLst;
            model.SymptomsLst = symptomsLst;

            return View(model);
        }


        // edit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id, IsSick, Status, TestDate, Temperature", "DoctorId", "StatusId", "Patient")] EditEventViewModel @event, List<int> SymptomsIds)
        {
            if (id != @event.Id)
            {
                return View("~/Views/Shared/Error.cshtml");
            }
            if(!(GetEruimByPermission().Cast<Event>().Any(e => e.Id == id)))
            {
                return View("~/Views/Shared/Error.cshtml");
            }
            // check if the user is sick
            else if(@event.IsSick)
            {
                var eventPatient = await _context.Event
                   .Include(u => u.Patient).ThenInclude(user=> user.City).FirstOrDefaultAsync(m => m.Id == id);

                _context.Entry(eventPatient).State = EntityState.Detached;
                _context.SaveChanges();
          
                await (new TwitterPostsController(_context)).NewUserInfectedPost(eventPatient.Patient);

            }

            if (ModelState.IsValid)
            {
                try
                {
                    Event newevent = new Event
                    {
                        Id = @event.Id,
                        Temperature = @event.Temperature,
                        IsSick = @event.IsSick,
                        TestDate = @event.TestDate,
                        UpdateTime = DateTime.Now,
                        Status = @event.Status,
                        Doctor = _context.User.FirstOrDefault(u => u.Id == @event.DoctorId),
                    };

                    _context.RemoveRange(_context.EventSymptoms.Where(es => es.EventId == newevent.Id));
                    foreach (var symptom in SymptomsIds)
                    {
                        var es = new EventSymptom() { SymptomId = symptom, EventId = newevent.Id };
                        _context.Add(es);
                    }
                    _context.SaveChanges();

                    _context.Update(newevent);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EventExists(@event.Id))
                    {
                        return View("~/Views/Shared/Error.cshtml");
                    }
                    else
                    {
                        throw;
                    }
                }

                return RedirectToAction(nameof(Index));
            }
            return View(@event);
        }

        // get delete view
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return View("~/Views/Shared/Error.cshtml");
            }

            var @event = await GetEruimByPermission().Cast<Event>()
                .FirstOrDefaultAsync(m => m.Id == id);

            if (@event == null)
            {
                return View("~/Views/Shared/Error.cshtml");
            }

            return View(@event);
        }

        // delete
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (!(GetEruimByPermission().Cast<Event>().Any(e => e.Id == id)))
            {
                return View("~/Views/Shared/Error.cshtml");
            }

            _context.RemoveRange(_context.EventSymptoms.Where(es => es.EventId == id));
            var @event = await _context.Event.FindAsync(id);
            _context.Event.Remove(@event);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EventExists(int id)
        {
            return _context.Event.Any(e => e.Id == id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Coronitor.Data;
using Coronitor.Models;

namespace Coronitor.Controllers
{
    public class JobTypesController : Controller
    {
        private readonly CoronitorContext _context;

        public JobTypesController(CoronitorContext context)
        {
            _context = context;

        }

        // GET: JobTypes
        public async Task<IActionResult> Index()
        {
            if (AccountController.IsUserConnected(this))
            {
                return View(await _context.JobType.ToListAsync());
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
           
        }

        // GET: JobTypes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (AccountController.IsUserConnected(this))
            {
                if (id == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                var jobType = await _context.JobType
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (jobType == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                return View(jobType);
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
           
        }

        // GET: JobTypes/Create
        public IActionResult Create()
        {
            if (AccountController.IsUserConnected(this))
            {
                return View();
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
          
        }

        // POST: JobTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name")] JobType jobType)
        {
            if (AccountController.IsUserConnected(this))
            {
                if (ModelState.IsValid)
                {
                    _context.Add(jobType);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                return View(jobType);
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
            
        }

        // GET: JobTypes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (AccountController.IsUserConnected(this))
            {
                if (id == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                var jobType = await _context.JobType.FindAsync(id);
                if (jobType == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }
                return View(jobType);
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
            
        }

        // POST: JobTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name")] JobType jobType)
        {
            if (AccountController.IsUserConnected(this))
            {
                if (id != jobType.Id)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        _context.Update(jobType);
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!JobTypeExists(jobType.Id))
                        {
                            return View("~/Views/Shared/Error.cshtml");
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return RedirectToAction(nameof(Index));
                }
                return View(jobType);
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
           
        }

        // GET: JobTypes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (AccountController.IsUserConnected(this))
            {
                if (id == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                var jobType = await _context.JobType
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (jobType == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                return View(jobType);
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
           
        }

        // POST: JobTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (AccountController.IsUserConnected(this))
            {
                var jobType = await _context.JobType.FindAsync(id);
                _context.JobType.Remove(jobType);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
            
        }

        private bool JobTypeExists(int id)
        {
            return _context.JobType.Any(e => e.Id == id);
        }
    }
}

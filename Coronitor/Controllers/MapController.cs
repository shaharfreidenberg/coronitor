﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Coronitor.Models;
using OpenCage.Geocode;

namespace Coronitor.Controllers
{
    public class MapController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public MapController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }


        public IActionResult GoogleMaps()
        {
            return View();
        }
        public IActionResult GetCities()
        {


            List<City> cities = new List<City>(){
        };

            // New thread for posting the twitt
            Task t = Task.Factory.StartNew(() =>
            {
                new TwitterController(_logger).TwwitTwit();

            });
            

            return new JsonResult(cities);

        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

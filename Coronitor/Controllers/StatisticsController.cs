﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Coronitor.Data;
using Coronitor.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ServiceStack;

namespace Coronitor.Controllers
{
    public class StatisticsController : Controller
    {
        private static int seq = 7000;
        private readonly CoronitorContext _context;
        public StatisticsController(CoronitorContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult GetTestsByMonth (string value)
        {
            var tests = _context.Event.ToList()
                .Where(x => x.TestDate != null)
                .Where(x => x.TestDate.ToString("yyyy-MM") == value)
                .GroupBy(x => x.TestDate.Day)
                .Select(x => new KeyValuePair<int, int>(x.Key, x.Count()));
            return new JsonResult(tests);
        }

        public IActionResult GetSickPatientsByAges ()
        {
            var res = _context.Event
                .Join(_context.User, e => e.Patient.Id, u => u.Id, (e, u) => new
                {
                    EventId = e.Id,
                    UserId = u.Id,
                    IsUserSick = e.IsSick,
                    UserAge = DateTime.Now.Year - u.Birthdate.Year
                })
                .ToList()
                .Where(x => x.IsUserSick)
                .GroupBy(x => x.UserAge)
                .Select(x => new KeyValuePair<int, int>(x.Key, x.Count()));
            return new JsonResult(res);
        }
        private int GetNextUserId()
        {
            if(seq == 7000 & _context.User.Count() > 0)
            {
                seq = _context.User.Max(x => x.Id);
            }
            return ++seq;
        }

        public void GenerateDataForStatistics()
        {
            var user1id = GetNextUserId();
            var user2id = GetNextUserId();
            var user3id = GetNextUserId();
            var user4id = GetNextUserId();
            var user5id = GetNextUserId();

            //0-9
            var user1 = new User
            {
                Id = user1id,
                FirstName = "user" + user1id,
                LastName = "user" + user1id,
                Birthdate = new DateTime(2019, 07, 16)
            };
            //20-29
            var user2 = new User
            {
                Id = user2id,
                FirstName = "user2" + user2id,
                LastName = "user2" + user2id,
                Birthdate = new DateTime(1996, 07, 16)
            };
            //70-79
            var user3 = new User
            {
                Id = user3id,
                FirstName = "user3" + user3id,
                LastName = "user3" + user3id,
                Birthdate = new DateTime(1945, 07, 16)
            };
            //80+
            var user4 = new User
            {
                Id = user4id,
                FirstName = "user4" + user4id,
                LastName = "user4" + user4id,
                Birthdate = new DateTime(1940, 07, 16)
            };
            //50-59
            var user5 = new User
            {
                Id = user5id,
                FirstName = "user5" + user5id,
                LastName = "user5" + user5id,
                Birthdate = new DateTime(1970, 07, 16)
            };

       
            _context.Add(new Event
            {
                Patient = user1,
                IsSick = true,
                TestDate = new DateTime(2020, 04, 05)
            }); 
            _context.Add(new Event
            {
                Patient = user2,
                IsSick = true,
                TestDate = new DateTime(2020, 04, 06)
            });
            _context.Add(new Event
            {
                Patient = user3,
                IsSick = true,
                TestDate = new DateTime(2020, 09, 07)
            });
            _context.Add(new Event
            {
                Patient = user4,
                IsSick = true,
                TestDate = new DateTime(2020, 09, 26)
            });
            _context.Add(new Event
            {
                Patient = user5,
                IsSick = true,
                TestDate = new DateTime(2020, 09, 06)
            });
            _context.Add(new Event
            {
                Patient = user5,
                IsSick = true,
                TestDate = new DateTime(2020, 08, 22)
            });
            _context.Add(new Event
            {
                Patient = user5,
                IsSick = true,
                TestDate = new DateTime(2020, 05, 11)
            });

            _context.SaveChanges();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Coronitor.Data;
using Coronitor.Models;

namespace Coronitor.Controllers
{
    public class SymptomsController : Controller
    {
        private readonly CoronitorContext _context;

        public SymptomsController(CoronitorContext context)
        {
            _context = context;
        }

        // GET: Symptoms
        public async Task<IActionResult> Index()
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                return View(await _context.Symptom.ToListAsync());
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
          
        }

        // GET: Symptoms/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                if (id == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                var symptom = await _context.Symptom
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (symptom == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                return View(symptom);
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
           
        }

        // GET: Symptoms/Create
        public IActionResult Create()
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                return View();
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
           
        }

        // POST: Symptoms/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name")] Symptom symptom)
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                if (ModelState.IsValid)
                {
                    _context.Add(symptom);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                return View(symptom);
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
           
        }

        // GET: Symptoms/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                if (id == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                var symptom = await _context.Symptom.FindAsync(id);
                if (symptom == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }
                return View(symptom);
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
           
        }

        // POST: Symptoms/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name")] Symptom symptom)
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                if (id != symptom.Id)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        _context.Update(symptom);
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!SymptomExists(symptom.Id))
                        {
                            return View("~/Views/Shared/Error.cshtml");
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return RedirectToAction(nameof(Index));
                }
                return View(symptom);
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
           
        }

        // GET: Symptoms/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                if (id == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                var symptom = await _context.Symptom
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (symptom == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                return View(symptom);
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
           
        }

        // POST: Symptoms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                var symptom = await _context.Symptom.FindAsync(id);
                _context.Symptom.Remove(symptom);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
            
        }

        private bool SymptomExists(int id)
        {
            return _context.Symptom.Any(e => e.Id == id);
        }
    }
}

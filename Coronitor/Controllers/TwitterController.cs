﻿using System.IO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Coronitor.Models;
using Tweetinvi;
using Tweetinvi.Parameters;
using Tweetinvi.Models;

namespace Coronitor.Controllers
{
    public class TwitterController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private static readonly string CONSUMER_KEY = "kAuwNCvr59W2AHAYvIPtLTHdH";
        private static readonly string CONSUMER_SECRET = "OmVkVrVxf7ckpYTQxhU7KcDxe8EvOV1qFEtvSv0vOFyH6qRbKm";
        private static readonly string USER_ACCESS_TOKEN = "1274249583246393344-ujKqSIXMLFyTklgNsxio1N2N5QxZN1";
        private static readonly string USER_ACCESS_SECRET = "cfyItF3b7JaoOXWG73zD9YrPDZrJwrqYHKabC4MMCMGlj";

        public TwitterController(ILogger<HomeController> logger):base()
        {
            _logger = logger;
            // set the configuration for the twitter api connection
          

        }
        public TwitterController()
        {
            // set the configuration for the twitter api connection
            Auth.SetUserCredentials
                (CONSUMER_KEY, CONSUMER_SECRET, USER_ACCESS_TOKEN, USER_ACCESS_SECRET);

        }

       
            public void InformAboutNewInfectionInCity(City city)
        {
            // Get city Coordinates
            Coordinates cityLoc = new Coordinates(city.Latitude, city.Longitude);

            // Load twit image
            byte[] twitImage = System.IO.File.ReadAllBytes("wwwroot/images/newCaseImage.jpg");
            var media = Upload.UploadBinary(twitImage);

            
            string tweetMsg = "התגלה חולה קורונה חדש בעיר" + city.Name;
            var tweet = Tweet.PublishTweet(tweetMsg,
                new PublishTweetOptionalParameters
                {
                    Coordinates = cityLoc,
                    Medias = new List<IMedia> { media }
                });
        }
        public IActionResult TwwitTwit()
        {
            var userIdentifier = new UserIdentifier("BatelDaudi1");
            var user = Tweetinvi.User.GetUserFromScreenName("BatelDaudi1");

            IMessage message = Message.PublishMessage("היי, אל תהיה מודאג אבל התגלה חולה קורונה בעיר שלך!;",1276772844060446721);


            return View();
        }
        public IActionResult GetCities()
        {
            List<City> cities = new List<City>(){
         
        };

            return new JsonResult(cities);
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

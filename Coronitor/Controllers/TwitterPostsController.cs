﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Coronitor.Data;
using Coronitor.Models;
using Tweetinvi;
using Tweetinvi.Models;
using Tweetinvi.Parameters;

namespace Coronitor.Controllers
{
    public class TwitterPostsController : Controller
    {
        private static readonly string CONSUMER_KEY = "kAuwNCvr59W2AHAYvIPtLTHdH";
        private static readonly string CONSUMER_SECRET = "OmVkVrVxf7ckpYTQxhU7KcDxe8EvOV1qFEtvSv0vOFyH6qRbKm";
        private static readonly string USER_ACCESS_TOKEN = "1274249583246393344-ujKqSIXMLFyTklgNsxio1N2N5QxZN1";
        private static readonly string USER_ACCESS_SECRET = "cfyItF3b7JaoOXWG73zD9YrPDZrJwrqYHKabC4MMCMGlj";
        private readonly CoronitorContext _context;

        public TwitterPostsController(CoronitorContext context)
        {
            _context = context;
            // set the configuration for the twitter api connection
            if(Auth.ApplicationCredentials == null)
            {
                Auth.SetUserCredentials
                (CONSUMER_KEY, CONSUMER_SECRET, USER_ACCESS_TOKEN, USER_ACCESS_SECRET);
            }
        }

        public async Task NewUserInfectedPost(Models.User user)
        {
            //int a = 
            // Check if we have a post about this user
            if (_context.TwitterPost.Count(x => x.InfectedUser.Id == user.Id) == 0)
            {
                await InformAboutNewInfectionInCityAsync(user);
            }
        }
        public async Task InformAboutNewInfectionInCityAsync(Models.User user)
        {
            City city = user.City;

            // Get city Coordinates
            Coordinates cityLoc = new Coordinates(city.Latitude, city.Longitude);

            // Load twit image
            byte[] twitImage = System.IO.File.ReadAllBytes("wwwroot/images/newCaseImage.jpg");
            var media = Upload.UploadBinary(twitImage);


            string tweetMsg = "התגלה חולה קורונה חדש בעיר - " + city.Name;
            var tweet = Tweet.PublishTweet(tweetMsg,
                new PublishTweetOptionalParameters
                {
                    Coordinates = cityLoc,
                    Medias = new List<IMedia> { media }
                });

           
            _context.TwitterPost.Add(new TwitterPost
            {
                InfectedUser = user,
                PostID = tweet.IdStr,
                PostUploadTime = DateTime.Now
            });

            await _context.SaveChangesAsync();
        }

        public void DeleteTwitterPost(string postID)
        {
            Tweet.DestroyTweet(long.Parse(postID));
        }
            // GET: TwitterPosts
        public async Task<IActionResult> Index(string firstName, string lastName, DateTime postDate)
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                firstName = (firstName == null ? "" : firstName);
                lastName = (lastName == null ? "" : lastName);

                var twitterPosts =
                    _context.TwitterPost.Include(post => post.InfectedUser).Where(s => s.InfectedUser.FirstName.Contains(firstName) &&
                    s.InfectedUser.LastName.Contains(lastName) && s.PostUploadTime >= postDate);

                return View(await twitterPosts.ToListAsync());
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
           
        }

        // GET: TwitterPosts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                if (id == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                var twitterPost = await _context.TwitterPost.Include(post => post.InfectedUser)
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (twitterPost == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                return View(twitterPost);
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
           
        }

        // GET: TwitterPosts/Create
        public IActionResult Create()
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                return View();
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
           
        }

        // POST: TwitterPosts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,PostID")] TwitterPost twitterPost)
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                if (ModelState.IsValid)
                {
                    _context.Add(twitterPost);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                return View(twitterPost);
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
           
        }

        // GET: TwitterPosts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                if (id == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                var twitterPost = await _context.TwitterPost.FindAsync(id);
                if (twitterPost == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }
                return View(twitterPost);
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
           
        }

        // POST: TwitterPosts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,PostID")] TwitterPost twitterPost)
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                if (id != twitterPost.Id)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        _context.Update(twitterPost);
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!TwitterPostExists(twitterPost.Id))
                        {
                            return View("~/Views/Shared/Error.cshtml");
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return RedirectToAction(nameof(Index));
                }
                return View(twitterPost);
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
          
        }

        // GET: TwitterPosts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                if (id == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                var twitterPost = await _context.TwitterPost
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (twitterPost == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                return View(twitterPost);
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
            
        }

        // POST: TwitterPosts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                var twitterPost = await _context.TwitterPost.FindAsync(id);
                DeleteTwitterPost(twitterPost.PostID);
                _context.TwitterPost.Remove(twitterPost);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
            
        }

        private bool TwitterPostExists(int id)
        {
            return _context.TwitterPost.Any(e => e.Id == id);
        }
    }
}

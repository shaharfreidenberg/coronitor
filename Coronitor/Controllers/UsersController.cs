﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Coronitor.Data;
using Coronitor.Models;
using Microsoft.AspNetCore.Http;

namespace Coronitor.Controllers
{
    public class UsersController : Controller
    {
        private readonly CoronitorContext _context;

        public UsersController(CoronitorContext context)
        {
            _context = context;
            
            if (_context.JobType.ToList().Count == 0)
            {
                _context.Database.ExecuteSqlCommand("DELETE FROM dbo.JobType");
                _context.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('dbo.JobType', RESEED, 1)");
                context.SaveChanges();
                foreach (var jobType in Enum.GetValues(typeof(JobTypeEnum)))
                {
                    // TODO DELTETE
                    _context.JobType.Add(new JobType
                    {
                        Name = jobType.ToString(),
                    });
                }
                _context.City.Add(new City
                {
                    Name = "חדרה",
                    Id = "1",
                    Longitude = 0,
                    Latitude = 0,
                });
            }

            _context.SaveChanges();
        }

        // GET: Users
        public async Task<IActionResult> Index()
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                return View(await _context.User.Include(u => u.JobType).Include(u => u.City).ToListAsync());
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
          
        }

        // GET: Users/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                if (id == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                var user = await _context.User.Include(u => u.JobType).Include(u => u.City)
                    .FirstOrDefaultAsync(m => m.Id == id);

                if (user == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                return View(user);
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
           
        }

        // GET: Users/Create
        public IActionResult Create()
        {

            //if (AccountController.IsAdminUserConnected(this))
            //{
                ViewBag.Jobs = new SelectList(_context.JobType.ToList(), "Id", "Name");
                ViewBag.Cities = new SelectList(_context.City.ToList(), "Id", "Name");
                ViewBag.isUserExists = false;
                return View();
            //}
            //else
            //{
                //return View("~/Views/Shared/401Error.cshtml");
            //}
            
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FirstName,LastName,Password,Birthdate")] User user, int JobId, string CityId)
        {
            /*if (AccountController.IsAdminUserConnected(this))
            {
                if (ModelState.IsValid)
                {
                    user.JobType = _context.JobType.First(job => job.Id == JobId);
                    user.City = _context.City.First(city => city.Id == CityId);
                    user.Admin = false;
                    _context.Add(user);
                    await _context.SaveChangesAsync();

                    HttpContext.Session.SetString("id", user.Id.ToString());

                    HttpContext.Session.SetString("firstname", user.FirstName);
                    HttpContext.Session.SetInt32("jobtype", user.JobType.Id);
                    return Redirect("~/Home/Index");
                }
                return View(user);
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }*/

            if (ModelState.IsValid)
            {
                if(UserExists(user.Id))
                {
                    ViewBag.isUserExists = true;
                    ViewBag.Jobs = new SelectList(_context.JobType.ToList(), "Id", "Name");
                    ViewBag.Cities = new SelectList(_context.City.ToList(), "Id", "Name");
                }
                else
                {
                    user.JobType = _context.JobType.First(job => job.Id == JobId);
                    user.City = _context.City.First(city => city.Id == CityId);
                    user.Admin = false;
                    _context.Add(user);
                    await _context.SaveChangesAsync();

                    HttpContext.Session.SetInt32("id", user.Id);

                    HttpContext.Session.SetString("firstname", user.FirstName);
                    HttpContext.Session.SetInt32("jobtype", user.JobType.Id);
                    return Redirect("~/Home/Index");
                }
                
            }
            return View(user);
        }

        // GET: Users/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                if (id == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                var user = await _context.User.FindAsync(id);
                if (user == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }
                return View(user);
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
            
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,FirstName,LastName,Password,Admin")] User user)
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                if (id != user.Id)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        _context.Update(user);
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!UserExists(user.Id))
                        {
                            return View("~/Views/Shared/Error.cshtml");
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return RedirectToAction(nameof(Index));
                }
                return View(user);
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
           
        }

        // GET: Users/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                string failed = "userDeletedFailed" + id;
                ViewData.Add(failed, false);

                if (id == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                var user = await _context.User
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (user == null)
                {
                    return View("~/Views/Shared/Error.cshtml");
                }

                return View(user);
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
            

        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (AccountController.IsAdminUserConnected(this))
            {
                var user = await _context.User.FindAsync(id);

                try
                {
                    _context.User.Remove(user);
                    _context.SaveChanges();

                }
                catch (Exception)
                {
                    string failed = "userDeletedFailed" + id.ToString();
                    ViewData.Add(failed, true);
                    return View(user);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return View("~/Views/Shared/401Error.cshtml");
            }
            

        }

        private bool UserExists(int id)
        {
            return _context.User.Any(e => e.Id == id);
        }
    }
}

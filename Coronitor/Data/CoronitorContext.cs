﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Coronitor.Models;

namespace Coronitor.Data
{
    public class CoronitorContext : DbContext
    {
        public CoronitorContext (DbContextOptions<CoronitorContext> options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EventSymptom>().HasKey(es => new { es.EventId, es.SymptomId });
            modelBuilder.Entity<EventSymptom>()
                .HasOne(es => es.Event)
                .WithMany(e => e.Symptoms)
                .HasForeignKey(es => es.EventId);
            modelBuilder.Entity<EventSymptom>()
                .HasOne(es => es.Symptom)
                .WithMany(s => s.Events)
                .HasForeignKey(es => es.SymptomId);
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Coronitor.Models.User> User { get; set; }
        public DbSet<Coronitor.Models.JobType> JobType { get; set; }
        public DbSet<Coronitor.Models.City> City { get; set; }
        public DbSet<Coronitor.Models.Event> Event { get; set; }
        public DbSet<Coronitor.Models.Symptom> Symptom { get; set; }
        public DbSet<Coronitor.Models.TwitterPost> TwitterPost { get; set; }
        public DbSet<EventSymptom> EventSymptoms { get; set; }

    }


}

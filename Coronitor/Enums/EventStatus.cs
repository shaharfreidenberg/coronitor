﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Coronitor.Models
{
    public enum EventStatus
    {
        [Display(Name = "New")]
        New,
        [Display(Name = "Awaiting Result")]
        AwaitingResult,
        [Display(Name = "Closed")]
        Closed
    }
}

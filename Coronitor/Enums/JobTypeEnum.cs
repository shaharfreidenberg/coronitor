﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coronitor.Models
{
    public enum JobTypeEnum
    {
        Doctor = 1,
        Patient = 2,
        LaboratoryDirector = 3
    }
}

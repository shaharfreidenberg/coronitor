﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Coronitor.Migrations
{
    public partial class q : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "ChanchToBeSick",
                table: "Event",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ChanchToBeSick",
                table: "Event");
        }
    }
}

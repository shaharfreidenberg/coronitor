﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Coronitor.Migrations
{
    public partial class shaharmig8 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ChanchToBeSick",
                table: "Event");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "ChanchToBeSick",
                table: "Event",
                type: "float",
                nullable: false,
                defaultValue: 0.0);
        }
    }
}

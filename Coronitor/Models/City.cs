using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Coronitor.Models
{
    public class City
    {
        public City()
        {
           
        }
  
        [Display(Name = "City Name")]
        public string Name { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string Id { get; set; }
    }
}

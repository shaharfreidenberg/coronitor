﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Coronitor.Models
{
    public class EditEventViewModel
    {
        [Display(Name = "Event Id")]
        public int Id { get; set; }
        public User Patient { get; set; }
        [Display(Name = "Doctor")]
        public int DoctorId { get; set; }
        [Display(Name = "Symptoms")]
        public int[] SymptomsIds { set; get; }
        public double Temperature { set; get; }
        [Display(Name = "Is Patient Sick")]
        public Boolean IsSick { get; set; }
        [Display(Name = "Status")]
        public EventStatus Status { get; set; }
        [Display(Name = "Test Date")]
        public DateTime TestDate { get; set; }
        public List<SelectListItem> DoctorsLst { get; set; }
        public List<SelectListItem> StatusesLst { get; set; }
        public List<SelectListItem> SymptomsLst { get; set; }
    }
}

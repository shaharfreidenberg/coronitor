﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Coronitor.Models
{
    public class Event
    {
        [Display(Name = "Event Id")]
        public int Id { get; set; }
        [Display(Name = "Creation Time")]
        public DateTime CreationTime { get; set; }
        [Display(Name = "Update Time")]
        public DateTime UpdateTime { get; set; }
        public User Patient { get; set; }
        public User Doctor { get; set; }
        public List<EventSymptom> Symptoms { set; get; }
        public double Temperature { set; get; }
        [Display(Name = "Is Patient Sick")]
        public Boolean IsSick { get; set; }


        [Display(Name = "Sickness likelihood")]
        [NotMapped]
        public double ChanchToBeSick { get; set; }
        public EventStatus Status { get; set; }
        [Display(Name = "Test Date")]
        public DateTime TestDate { get; set; }
        [NotMapped]
        public string SymptomsString { 
            get { 
                if(Symptoms != null && Symptoms.Count() > 0)
                {
                    return string.Join(", ", Symptoms.Select(s => s.Symptom.Name));
                }
                return "no symptoms";
            } 
        }
        [NotMapped]
        public string ChanceToBeSickString
        {
            get
            {
                if(ChanchToBeSick == -1)
                {
                    return "no enough data to predict";
                }
                return ChanchToBeSick + "%";
            }
        } 
    }
}

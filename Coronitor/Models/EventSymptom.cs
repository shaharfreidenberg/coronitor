﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coronitor.Models
{
    public class EventSymptom
    {
        public Event Event { get; set; }
        public Symptom Symptom { get; set; }
        public int EventId { get; set; }
        public int SymptomId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Coronitor.Models
{
    public class LoginViewModel
    {
        [Required]
        [RegularExpression(@"^(\d{9})$", ErrorMessage = "Please enter proper ID")]
        public int Id { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}

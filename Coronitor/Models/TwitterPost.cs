﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Coronitor.Models
{
    public class TwitterPost
    {
        public int Id { get; set; }
        [Display(Name = "Post ID")]
        public string PostID { get; set; }
        [Display(Name = "Sick Patient")]
        public User InfectedUser { get; set; }

        [Display(Name = "Post Upload Date")]
        public DateTime PostUploadTime { get; set; }
    }
}

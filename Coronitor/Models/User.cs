﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Coronitor.Models
{
    public class User
    {
        [Display(Name = "User Id")]
        [RegularExpression(@"^(\d{9})$", ErrorMessage = "Please enter proper ID")]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name="Last Name")]
        public string LastName { get; set; }

        [RegularExpression("(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,15})$", ErrorMessage = "Password must be between 8 and 15 characters long and contain at least one number, one uppercase letter and one lowercase letter")]
        public string Password { get; set; }

        public Boolean Admin { get; set; }
        public JobType JobType { get; set; }
        public City City { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{yyyy-MM-dd}")]
        [Display(Name = "Birthdate")]
        public DateTime Birthdate { get; set; }
        [NotMapped]
        [Display(Name = "Full Name")]
        public string FullName
        {
            get { return FirstName + " " + LastName; }
        }
    }
}

﻿function generatePie(svg_id, data, prop) {

    var svg = d3.select(svg_id),
        width = svg.attr("width"),
        height = svg.attr("height"),
        radius = Math.min(width, height) / 2,
        g = svg.append("g").attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

    var color = d3.scaleOrdinal(['#17a2b8', '#ffc107', '#dc3545', '#20c997', '#6610f2']);

    // Generate the pie
    var pie = d3.pie().value(function (d) {
        return d.percent;
    });

    // Generate the arcs
    var path = d3.arc()
        .innerRadius(0)
        .outerRadius(radius - 10);

    var label = d3.arc()
        .outerRadius(radius)
        .innerRadius(radius - 80);


    //Generate groups
    var arcs = g.selectAll("arc")
        .data(pie(data))
        .enter()
        .append("g")
        .attr("class", "arc")
        // tooltip
        .on("mouseover", function (d) {
            d3.select("#tooltip")
                .style("left", d3.event.pageX + "px")
                .style("top", d3.event.pageY + "px")
                .style("opacity", 1)
                .select("#percent")
                .text(d.data.percent.toFixed(2));
            d3.select("#tooltip")
                .select("#count")
                .text(d.data.count);
        })
        .on("mouseout", function () {
            d3.select("#tooltip")
                .style("opacity", 0);;
        });


    //Draw arc paths
    arcs.append("path")
        .attr("d", path)
        .attr("fill", function (d) {
            return color(d.data[prop]);
        });

    arcs.append("text")
        .attr("transform", function (d) {
            return "translate(" + label.centroid(d) + ")";
        })
        .text(function (d) { return d.data[prop]; });

    svg.style("opacity", 1);
    document.getElementById("ages-pie-title").style.opacity = 1;
}